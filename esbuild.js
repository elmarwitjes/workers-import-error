require('esbuild').build({
  bundle: true,
  entryPoints: ['workers-site/index.js'],
  outfile: 'dist/index.js',
  format: 'cjs',
  loader: {
    '.mustache': 'text'
  }
});